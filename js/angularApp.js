var app = angular.module('lxl-team', []);

app.controller('TeamCtrl', [
	'$scope',
	function($scope){
		$scope.employees = [
			{id:1,photo:'syedsultanahmed.jpg',name:'Sultan',designation:'Managing Director',department:'LXL Ideas', in:'https://in.linkedin.com/in/syedsultanahmed',fb:'na',tw:'na'},
			{id:2,photo:'nehajain.jpg',name:'Neha',designation:'Head',department:'LXL Films', in:'https://in.linkedin.com/in/nehajain29',fb:'na',tw:'na'},
			{id:3,photo:'kalpakartik.jpg',name:'Kalpa',designation:'Head',department:'LXL Think', in:'https://in.linkedin.com/in/kalpa-kartik-06049ba7',fb:'na',tw:'na'},
			{id:4,photo:'maheshvbellur.jpg',name:'Mahesh',designation:'National Business Head',department:'LXL Ideas', in:'https://in.linkedin.com/in/mahesh-v-bellur-03a4ab14',fb:'na',tw:'na'},
			// {id:5,photo:'Placeholder_person.png',name:'Mallik',designation:'Vice President',department:'LXL Ideas', in:'&nbsp;',fb:'na',tw:'na'},
			{id:6,photo:'praveenum.jpg',name:'Praveen',designation:'Sr. Manager',department:'LXL Teach & LXL SP', in:'https://in.linkedin.com/in/praveen-um-331bb39a',fb:'na',tw:'na'},
			// {id:7,photo:'kavanabasavaraja.jpg',name:'Kavana',designation:'Operations Head',department:'Krayon', in:'https://in.linkedin.com/in/kavanabasavaraj',fb:'na',tw:'na'},
			{id:7,photo:'khalidahmedv.jpg',name:'Khalid',designation:'Sr. Manager',department:'LXL People', in:'https://in.linkedin.com/in/khalid-ahmed-414827b9',fb:'na',tw:'na'},
			{id:8,photo:'chironjitganguly.jpg',name:'Chironjeet',designation:'Sr.Relationship Manager',department:'LXL Biz', in:'https://in.linkedin.com/in/chironjit-ganguly-27b0bb21',fb:'na',tw:'na'},
			{id:9,photo:'kasturirangabs.jpg',name:'Kasturi',designation:'Manager',department:'LXL SP & IT', in:'https://in.linkedin.com/in/kasturi-ranga-bs-29086a50',fb:'na',tw:'na'},
			{id:10,photo:'chaitanyadeshpande.jpg',name:'Chaitanya',designation:'Manager',department:'LXL Think', in:'https://in.linkedin.com/in/findchaitanyahere',fb:'na',tw:'na'},
			// {id:12,photo:'arshiyasultana.jpg',name:'Arshiya',designation:'Manager',in:'&nbsp;',fb:'na',tw:'na'},
			{id:11,photo:'ankitvaish.jpg',name:'Ankit',designation:'Sr.Relationship Manager',department:'LXL Biz', in:'https://in.linkedin.com/in/ankit-vaish-67aa7954',fb:'na',tw:'na'},
			{id:12,photo:'arunb.p..jpg',name:'Arun',designation:'Asst. Manager',department:'LXL Teach',in:'https://in.linkedin.com/in/arun-arun-84269334',fb:'na',tw:'na'},
			{id:13,photo:'gururajnaikas.jpg',name:'Gururaj',designation:'Sr. Executive',department:'LXL Teach', in:'https://in.linkedin.com/in/gururaj-nail-a-s-0460a688',fb:'na',tw:'na'},
			// {id:15,photo:'harimohansingh.jpg',name:'Hari',designation:'Relationship Manager',department:'LXL Think', in:'in/hari-mohan-singh-4b928295',fb:'na',tw:'na'},
			{id:14,photo:'harmanpreetkaur.jpg',name:'Harmanpreet',designation:'Asst. Manager',department:'LXL Films', in:'https://in.linkedin.com/in/harmankaur',fb:'na',tw:'na'},
			{id:15,photo:'harpreetsingh.jpg',name:'Harpreet',designation:'Asst. Manager',department:'LXL Think', in:'https://in.linkedin.com/in/harpreetrock',fb:'na',tw:'na'},
			{id:16,photo:'mohammedmohitkhan.jpg',name:'Mohit',designation:'Manager',department:'Krayon',in:'https://in.linkedin.com/in/mohdmohitkhan',fb:'na',tw:'na'},
			{id:17,photo:'nandithasathish.jpg',name:'Nanditha',designation:'Sr. Executive',department:'LXL Teach', in:'https://in.linkedin.com/in/nanditha-sathish-95a0798a',fb:'na',tw:'na'},
			// {id:19,photo:'piyushgarud.jpg',name:'Piyush',designation:'Sr. Executive',department:'LXL Films', in:'https://in.linkedin.com/in/piyush-garud-178b342b',fb:'na',tw:'na'},
			{id:18,photo:'pournamihs.jpg',name:'Pournami',designation:'Asst. Manager',department:'LXL People', in:'https://in.linkedin.com/in/pournami-hs-b6148666',fb:'na',tw:'na'},
			// {id:21,photo:'simrandeepkaurbanwait.jpg',name:'Simrandeep',designation:'Executive',department:'LXL Think', in:'https://in.linkedin.com/in/simran-banwait-5bb6b646',fb:'na',tw:'na'},
			{id:19,photo:'sushantraogeorge.jpg',name:'Sushant',designation:'Manager- Events& BD',department:'LXL Biz',in:'https://in.linkedin.com/in/sushantgeorge',fb:'na',tw:'na'},
			{id:20,photo:'trishapoddar.jpg',name:'Trisha',designation:'Sr.Relationship Manager',department:'LXL Biz',in:'https://in.linkedin.com/in/trisha-poddar-3b36b657',fb:'na',tw:'na'},
			{id:21,photo:'yashikabegwani.jpg',name:'Yashika',designation:'Sr. Executive',department:'LXL Think',in:'https://in.linkedin.com/in/yashika-begwani-9a869517',fb:'na',tw:'na'}
		];
}]);
